
# https://www.jb51.net/article/162585.htm
# pip install -i https://pypi.tuna.tsinghua.edu.cn/simple selenium


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import datetime
import time
import sys

## username
user = "username"
## password
pwd = "secret"

# 打开Chrome浏览器
browser = webdriver.Chrome()

browser.get("https://www.jd.com")
time.sleep(3)

browser.get("https://passport.jd.com/new/login.aspx?ReturnUrl=https%3A%2F%2Fwww.jd.com%2F")
browser.find_element_by_link_text("账户登录").click()


# https://selenium-python.readthedocs.io/locating-elements.html

elem = browser.find_element_by_id("loginname")
elem.send_keys(user)
elem = browser.find_element_by_id("nloginpwd")
elem.send_keys(pwd)
time.sleep(1)
browser.find_element_by_id("loginsubmit").click()

time.sleep(10)  #手工拼图登录, 等等10秒


#browser.get("https://item.jd.com/100011521400.html")  #振德 （ZHENDE) 口罩一次性医用口罩
browser.get("https://item.jd.com/100011551632.html")  #3Q医用口罩 透气轻薄成人3层医用一次性口罩 1袋10只
#browser.get("https://item.jd.com/100006394713.html")  #袋鼠医生（DR.ROOS）医用外科口罩 口罩 10只/包 3层无菌挂耳
#browser.refresh();

#buytime = "2020-03-08 10:30:00.002000"   #振德 （ZHENDE) 口罩一次性医用口罩
buytime = "2020-03-08 20:00:00.001000"   #3Q医用口罩 透气轻薄成人3层医用一次性口罩 1袋10只
#buytime = "2020-03-08 20:00:00.002000"   #袋鼠医生（DR.ROOS）医用外科口罩 口罩 10只/包 3层无菌挂耳


def buy(buytime):
    while True:
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
        sys.stdout.write("\rNow: {now} ==== buy time: {buytime}".format(now=now, buytime=buytime))
        sys.stdout.flush()
        
        # 对比时间，时间到的话就点击结算
        if now > buytime:
            print(">>>> start to buy");
            # 点击结算按钮
            try:
                browser.refresh();
                browser.find_element_by_link_text("立即抢购").click()
                browser.get("https://cart.jd.com/cart.action")
                browser.find_element_by_link_text("去结算").click()
                    
                # 点击提交订单按钮
                browser.find_element_by_id("order-submit").click()
                time.sleep(300)
            except:
                print(">>>> try again to buy");
                buy(buytime)
            
        time.sleep(0.001)


buy(buytime)


